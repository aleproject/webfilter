import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthModule } from '@auth0/auth0-angular';
import { MaterialModule } from './shared/material.module';

@NgModule({
  declarations: [AppComponent,],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AuthModule.forRoot({
        domain: 'ermes-test.eu.auth0.com',
        clientId: 'IM8ZJoPaxmasVUv6Jvp4JePBeTvSQFlh',
      }),
    MaterialModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
